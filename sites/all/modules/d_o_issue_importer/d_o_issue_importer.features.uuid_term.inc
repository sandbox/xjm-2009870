<?php
/**
 * @file
 * d_o_issue_importer.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function d_o_issue_importer_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => '9.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '301e5901-ac99-bb34-c1dd-1278365c399e',
    'vocabulary_machine_name' => 'core_version',
    'path' => array(
      'alias' => 'core-version/9x-dev',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => '8.x-1.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '3a42adf9-3094-48b4-15ee-19b436571036',
    'vocabulary_machine_name' => 'core_version',
    'path' => array(
      'alias' => 'core-version/8x-1x-dev',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => '7.x-1.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '4efb562d-ca8c-88c4-c524-9f47f1811444',
    'vocabulary_machine_name' => 'core_version',
    'path' => array(
      'alias' => 'core-version/7x-1x-dev',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => '7.x-3.0-beta11',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '7e0a35d6-709d-0794-6d42-e15d5ec80ec3',
    'vocabulary_machine_name' => 'core_version',
    'path' => array(
      'alias' => 'core-version/7x-30-beta11',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => '7.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '88dde4c6-6754-e1b4-c909-6b585751bd18',
    'vocabulary_machine_name' => 'core_version',
  );
  $terms[] = array(
    'name' => '7.x-2.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '9b8ab115-64ad-ace4-1dbb-216fec192d55',
    'vocabulary_machine_name' => 'core_version',
    'path' => array(
      'alias' => 'core-version/7x-2x-dev',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => '6.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'a9ae5847-a541-faf4-6d45-ebeecdd6db86',
    'vocabulary_machine_name' => 'core_version',
    'path' => array(
      'alias' => 'core-version/6x-dev',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => '7.x-3.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'aa386665-17b7-9384-b551-e005659bd341',
    'vocabulary_machine_name' => 'core_version',
    'path' => array(
      'alias' => 'core-version/7x-3x-dev',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => '8.x-3.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'b132d866-bf3f-eb34-b15d-094e81b99c02',
    'vocabulary_machine_name' => 'core_version',
    'path' => array(
      'alias' => 'core-version/8x-3x-dev',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => '8.x-dev',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'c72a3e12-a3be-3264-f1cc-ddddc0e79140',
    'vocabulary_machine_name' => 'core_version',
  );
  return $terms;
}
