<?php
/**
 * @file
 * d_o_issue_importer.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function d_o_issue_importer_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_d_o_issue';
  $strongarm->value = 0;
  $export['comment_anonymous_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_d_o_issue';
  $strongarm->value = 1;
  $export['comment_default_mode_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_d_o_issue';
  $strongarm->value = '50';
  $export['comment_default_per_page_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_d_o_issue';
  $strongarm->value = '1';
  $export['comment_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_d_o_issue';
  $strongarm->value = 1;
  $export['comment_form_location_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_d_o_issue';
  $strongarm->value = '1';
  $export['comment_preview_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_d_o_issue';
  $strongarm->value = 1;
  $export['comment_subject_field_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_d_o_issue';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_d_o_issue';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_d_o_issue';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_d_o_issue';
  $strongarm->value = '1';
  $export['node_preview_d_o_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_d_o_issue';
  $strongarm->value = 1;
  $export['node_submitted_d_o_issue'] = $strongarm;

  return $export;
}
