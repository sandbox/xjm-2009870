/**
 * @file
 * JavaScript for the Drupal.org issue importer.
 */

/**
 * Converts Drupal.org issue URLs into node IDs.
 */
(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.d_o_issue_importer = {
  attach: function (context, settings) {
    $('.change_nid').once().change(function(context) {
      if (matches = $(this).val().match('^https?://drupal.org/node/([0-9]+)')) {
        $(this).val(matches[1]);
      }
    });
  }
};

})(jQuery, Drupal, this, this.document);
