<?php
/**
 * @file
 * Code for the officehours_permissions feature.
 */

/**
 * Implements hook_permission().
 *
 * Grants permission to update others' task attempts and to create published
 * tasks.
 *
 * @see officehours_permissions_field_widget_field_collection_table_form_alter()
 * @see officehours_permissions_menu_alter()
 *
 * @todo This is a workaround for an incompatibility between field_collection
 *   and field_permissions. Fix it in field_collection instead.
 */
function officehours_permissions_permission() {
  return array(
    'update task information' => array(
      'title' => t('Update task information'),
    ),
    'update any task attempt' => array(
      'title' => t('Update any task attempt'),
    ),
    'set task status' => array(
      'title' => t('Set task status'),
    ),
    'create published tasks' => array(
      'title' => t('Create published task'),
      'description' => t('Tasks will be publshed on creation. (Tasks are unpublished by default.)'),
    ),
  );
}

/**
 * Implements hook_node_prepare().
 *
 * Makes task nodes default to 'published' when the user has the
 * 'create published tasks' permission.
 */
function officehours_permissions_node_prepare($node) {
  // Only act on new task nodes.
  if (($node->type != 'task') || !empty($node->nid)) {
    return;
  }
  // Only act when the user has the permission.
  if (!user_access('create published tasks')) {
    return;
  }

  // Make the node published by default.
  $node->status = NODE_PUBLISHED;
}

/**
 * Implements hook_form_FORM_ID_alter() for task_node_form.
 *
 * Disables task title and body fields for unprivileged users. (The title
 * cannot be disabled with hook_field_access(), and the body is shared across
 * content types.)
 *
 * @todo Make task notes a different field?
 *
 * @see officehours_permissions_field_access()
 */
function officehours_permissions_form_task_node_form_alter(&$form, &$form_state) {
  // Only act if the user does not have permission to update the task
  // information.
  if (user_access('update task information')) {
    return;
  }

  // Disable the title and body fields for existing nodes.
  if (!empty($form['#node']->nid)) {
    $form['title']['#disabled'] = TRUE;
    $form['body']['#disabled'] = TRUE;
  }
}

/**
 * Implements hook_field_access().
 *
 * Restricts access to edit certain task node fields.
 *
 * @see officehours_permissions_form_task_node_form_alter()
 */
function officehours_permissions_field_access($op, $field, $entity_type, $entity, $account) {
  // Don't alter view permissions.
  if ($op == 'view') {
    return TRUE;
  }
  // Only act on task nodes.
  if ($entity_type != 'node') {
    return TRUE;
  }
  if (!empty($entity) && $entity->type != 'task') {
    return TRUE;
  }
  // Take no action if the user has access to update task information.
  if (user_access('update task information', $account)) {
    return TRUE;
  }

  // Allow users with 'set task status' permission to set the followup field.
  if ($field == 'field_followup') {
    if (user_access('set task status', $account)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  // Restrict access to existing task information for unprivileged users.
  $restricted_fields = array(
    'field_task_d_o_issue',
    'field_task_type',
    'field_sprint',
    'field_blocker',
  );
  if (!empty($entity->nid) && in_array($field['field_name'], $restricted_fields)) {
    return FALSE;
  }

  // Allow access by default.
  return TRUE;
}

/**
 * Restricts access to task attempt elements and provides a default.
 *
 * Users without the 'update any task attempt' permission may only add or edit
 * task attempts that include them as participants.
 *
 * @see officehours_permissions_permission()
 * @see officehours_permissions_menu_alter()
 * @see officehours_permissions_field_widget_field_collection_table_form_alter()
 * @see officehours_permissions_form_field_collection_item_form_alter()
 *
 * @todo This is a workaround for an incompatibility between field_collection
 *   and field_permissions. Fix it in field_collection instead.
 */
function officehours_permission_set_attempt_permissions(&$element, $full_form = FALSE) {
  global $user;
  $langcode = $element['field_task_participant']['#language'];

  // Users with the 'update task attempt' permission have separate handling.
  if (user_access('update any task attempt')) {
    // If this is a new attempt, and no mentor is set, and the user is not the
    // site admin, populate a default value for the mentor.
    if (empty($element['#entity']->item_id) && empty($element['field_task_mentor'][$langcode]['#default_value']) && $user->uid != 1) {
      $element['field_task_mentor'][$langcode]['#default_value'] = $user->name . ' (' . $user->uid . ')';
    }

    // Return without altering access to the field.
    return;
  }

  // Otherwise, if the user ID is not equal to the participant ID, disable
  // all elements for this delta.
  // Match the paticipant ID from the field value. I really hate the way
  // entityreference does its autocomplete.
  $participant_ids = array();
  if (!empty($element['field_task_participant'][$langcode]['#default_value'])) {
    $participant_ids = officehours_task_entityreference_autocomplete_tag_ids($element['field_task_participant'][$langcode]['#default_value'], 'field_task_participant');
  }
  else {
    $element['field_task_participant'][$langcode]['#default_value'] = $user->name . ' (' . $user->uid . ')';
  }
  // If this user is not one of the participants, render as non-editable.
  if ((!empty($participant_ids)) && !in_array($user->uid, $participant_ids)) {
    $element['#disabled'] = TRUE;
  }
  elseif (!empty($element['#entity']->item_id) && empty($participant_ids)) {
    $element['#disabled'] = TRUE;
  }

  // Add a validator to enforce that the user must be a participant in changed
  // fields.
  $validate_key = $full_form ? '#validate' : '#element_validate';
  $validate = array('officehours_permissions_validate_participant');
  if (empty($element[$validate_key])) {
    $element[$validate_key] = $validate;
  }
  else {
    $element[$validate_key] = array_merge($validate, $element[$validate_key]);
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for field_collection_item.
 *
 * @see officehours_permission_set_attempt_permissions()
 */
function officehours_permissions_form_field_collection_item_form_alter(&$form, &$form_state, $form_id) {
  officehours_permission_set_attempt_permissions($form, TRUE);
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter() for field_collection_table.
 *
 * @see officehours_permission_set_attempt_permissions()
 */
function officehours_permissions_field_widget_field_collection_table_form_alter(&$element, &$form_state, $context) {
  // Only act on task attempt fields.
  if ($context['field']['field_name'] != 'field_task_attempt') {
    return;
  }
  officehours_permission_set_attempt_permissions($element);
}

/**
 * Form element validation handler for task attempts.
 *
 * @see officehours_permission_set_attempt_permissions()
 * @see officehours_permissions_field_attach_submit()
 */
function officehours_permissions_validate_participant(&$element, &$form_state, $form = '') {
  // Don't try to validate disabled elements.
  if (!empty($element['#disabled'])) {
    return;
  }
  global $user;
  $langcode = $element['field_task_participant']['#language'];
  // Fail validation if the current user is not listed as the participant.
  $participant_ids = officehours_task_entityreference_autocomplete_tag_ids($element['field_task_participant'][$langcode]['#value'], 'field_task_participant');
  if (!in_array($user->uid, $participant_ids)) {
      form_error($element['field_task_participant'], t('You must enter your username as one of the participants.'));
  }

  // If the participant has set the attempt to 'Work complete', flag the task
  // for mentor review.
  if (($element['field_progress'][$langcode]['#value'] == OFFICEHOURS_TASK_PROGRESS_COMLPETE) && ($element['field_progress'][$langcode]['#default_value'] != array(OFFICEHOURS_TASK_PROGRESS_COMLPETE))) {
    $form_state['#flag_for_review'] = TRUE;
  }
}

/**
 * Implements hook_field_attach_submit().
 *
 * Flags the task for mentor review if an unprivileged user marks a task
 * attempt as complete.
 *
 * @see officehours_permissions_validate_participant()
 */
function officehours_permissions_field_attach_submit($entity_type, $entity, $form, &$form_state) {
  // Only act on nodes.
  if ($entity_type != 'node') {
    return;
  }

  $langcode = $entity->language;

  // If processing indicates the task needs to be flagged for mentor review,
  // update the value.
  if (!empty($form_state['#flag_for_review'])) {
    $entity->field_followup[$langcode][0]['value'] = OFFICEHOURS_TASK_FOLLOWUP_REVIEW;
  }
}

/**
 * Implements hook_menu_alter().
 *
 * @see officehours_permissions_field_widget_field_collection_table_form_alter()
 * @see officehours_permissions_permission()
 *
 * @todo This is a workaround for an incompatibility between field_collection
 *   and field_permissions. Fix it in field_collection instead.
 */
function officehours_permissions_menu_alter(&$items) {
  $items['field-collection/field-task-attempt/%field_collection_item/edit']['access callback'] = 'officehours_permissions_attempt_access';
  $items['field-collection/field-task-attempt/%field_collection_item/delete']['access callback'] = 'officehours_permissions_attempt_access';
}

/**
 * Access callback: Determines if the current user has access to the attempt.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'.
 * @param $item
 *   Optionally a field collection item. If nothing is given, access for all
 *   items is determined.
 * @param $account
 *   The user to check for. Leave it to NULL to check for the global user.
 *
 * @return
 *   Whether access is allowed or not.
 *
 * @see officehours_permissions_attempt_access()
 * @see officehours_permissions_menu_alter()
 * @see field_collection_item_access()
 */
function officehours_permissions_attempt_access($op, FieldCollectionItemEntity $item = NULL, $account = NULL) {
  // Deny access if field_collection does.
  if (!field_collection_item_access($op, $item, $account)) {
    return FALSE;
  }
  // Allow access if the user has permission to update any task attempt.
  if (user_access('update any task attempt')) {
    return TRUE;
  }
  // Otherwise, check whether the user is a participant for the task.
  global $user;
  foreach ($item->field_task_participant as $participants) {
    foreach ($participants as $participant) {
      if ($participant['target_id'] == $user->uid) {
        return TRUE;
      }
    }
  }

  // Deny access if no participant matched.
  return FALSE;
}
