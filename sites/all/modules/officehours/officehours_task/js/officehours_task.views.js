/**
 * @file
 * JavaScript for the task listings.
 */

/**
 * Adds custom styling to the task listing based on field values.
 *
 * Yeah, lazy. Could use a refactor.
 */
(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.officehours_task = {
  attach: function (context, settings) {
    // Make certain status column values match d.o colors.
    var $status = $(context).find('td.task-status');
    $status.filter(":contains('needs work')").once('task-status').addClass('nw');
    $status.filter(":contains('needs review')").once('task-status').addClass('nr');
    $status.filter(":contains('postponed')").once('task-status').addClass('postponed');
    $status.filter(":contains('fixed')").once('task-status').addClass('fixed');
    $status.filter(":contains('by the community')").once('task-status').addClass('rtbc');
  }
};

})(jQuery, Drupal, this, this.document);
