<?php
/**
 * @file
 * officehours_task.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function officehours_task_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'munich2012',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '038504eb-6b13-2554-91e7-dd55ac49dd6e',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/munich2012',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'MumbaiApr13',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '24af994a-9bb8-eb94-4da3-054c9b96d26a',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/mumbaiapr13',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'MumbaiMar13',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '323ec444-6521-08e4-65a6-be438b992e33',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/mumbaimar13',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'DBUG',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '37025b2e-4e15-3ed4-7d19-f77182870e27',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/dbug',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'nyccamp2012',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '383ae41d-2bd6-40d4-0dc4-6103181ca28f',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/nyccamp2012',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Sydney2013',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '3c11d3fb-d30a-f2f4-11c6-ab66a5877c23',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/sydney2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Canberra2013',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '3d731acf-7b64-ba44-057a-cf8671b5ee80',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/canberra2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'APCO2013',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '435da335-7f4d-ec24-3d08-94b89d5c6af4',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/apco2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Burlington2013',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '52ab46bc-8376-b854-7d91-7efc55351c37',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/burlington2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'SacramentoDrupalSprint',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '6343c795-63f5-6114-a578-293e764f1b1f',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/sacramentodrupalsprint',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'chicago2013',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '8ccf48b8-5c83-c534-b19c-aff9f9a77ec3',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/chicago2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'mwds2012',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '93e00743-01b0-b1f4-31d1-83a2c54d368c',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/mwds2012',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'londugmarch2013',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '9cefb1e7-0b65-9cb4-cd29-c10ae376f2d7',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/londugmarch2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Portland2013',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '9ecbeb35-72ee-d114-dda8-9dd09dc0d3d8',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/portland2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'chennaiinda2013 tasks',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'a1052fb6-8cc0-db04-61b2-80af0b5d9250',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/chennaiinda2013-tasks',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'vanmarch2013',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'a1324718-ea26-5694-6576-36b15e8c0b01',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/vanmarch2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'dcatlanta2012',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'a2a8befb-2d60-3b94-7d17-6ea8c659fac8',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/dcatlanta2012',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'capitalcamp2012',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'a7fcf8b0-c103-3cc4-9dc9-5070c6e161ca',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/capitalcamp2012',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'drupaldelphia2012',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'b4df9778-0576-e8e4-fd24-5a94dae6dec5',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'london092012',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'b8ee15a3-ea75-b6d4-a130-f5a2ea97e345',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/london092012',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'londugmay2013',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'b9c76caf-477f-0b54-0d10-2e3753917b93',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/londugmay2013',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'MumbaiMay13',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'cdb3f8ef-eb5b-01c4-75e0-ce97fee0720f',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/mumbaimay13',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'BrightonUKSprint',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'dbf5df3e-9da4-54b4-5176-9be8a75b32b5',
    'vocabulary_machine_name' => 'sprint',
    'field_inactive' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'sprint/brightonuksprint',
      'pathauto' => FALSE,
    ),
  );
  return $terms;
}
