<?php
/**
 * @file
 * Code for the officehours_task feature.
 */

include_once 'officehours_task.features.inc';

/**
 * Value for field_progress: No current work.
 */
const OFFICEHOURS_TASK_PROGRESS_NO_WORK = 0;

/**
 * Value for field_progress: Work in progress.
 */
const OFFICEHOURS_TASK_PROGRESS_CURRENT_WORK = 1;

/**
 * Value for field_progress: Work complete.
 */
const OFFICEHOURS_TASK_PROGRESS_COMLPETE = 2;

/**
 * Value for field_followup: Awaiting followup.
 */
const OFFICEHOURS_TASK_FOLLOWUP_WAIT = 0;

/**
 * Value for field_followup: Needs mentor review.
 */
const OFFICEHOURS_TASK_FOLLOWUP_REVIEW = 1;

/**
 * Value for field_followup: Task complete.
 */
const OFFICEHOURS_TASK_FOLLOWUP_COMPLETE = 1;

/**
 * Value for field_followup: Not currently relevant.
 */
const OFFICEHOURS_TASK_FOLLOWUP_NOT_RELEVANT = -1;

/**
 * Extracts entity IDs from an entityreference tag autocomplete field value.
 *
 * @param $value
 *   Value from the autocomplete field, e.g.:
 *   'entity one (1), entity two (2)'
 *
 * @return
 *   An array of entity IDs.
 *
 * @see _entityreference_autocomplete_tags_validate()
 *
 * @todo This should be entityreference API.
 */
function officehours_task_entityreference_autocomplete_tag_ids($value, $field_name = '') {
  if (empty($value)) {
    return array();
  }
  $entity_ids = array();
  $entities = drupal_explode_tags($value);
  foreach ($entities as $entity) {
    // Given "label (entity ID)", match the ID from parentheses.
    if (preg_match("/.+\((\d+)\)/", $entity, $matches)) {
      $entity_ids[] = $matches[1];
    }
    // If no entity ID was matched, try looking up the entity.
    elseif (!empty($field_name)) {
      $field = field_info_field($field_name);
      $handler = entityreference_get_selection_handler($field);
      $matched_entities = $handler->getReferencableEntities($entity, '=', 2);
      if (is_array($matched_entities) && count($matched_entities) === 1) {
        $entity_ids[] = key($matched_entities);
      }
    }
  }
  return $entity_ids;
}

/**
 * Implements hook_entity_property_info_alter().
 */
function officehours_task_entity_property_info_alter(&$info) {
  $properties = &$info['node']['bundles']['task']['properties'];
  $properties['all_participants'] = array(
    'type' => 'user',
    'label' => t('Participants'),
    'getter callback' => 'officehours_task_all_participants',
    'computed' => TRUE,
    'entity views field' => TRUE,
    'description' => t('A list of all participants'),
  );
  $properties['all_mentors'] = array(
    'type' => 'user',
    'label' => t('Mentors'),
    'getter callback' => 'officehours_task_all_mentors',
    'computed' => TRUE,
    'entity views field' => TRUE,
    'description' => t('A list of all mentors'),
  );
  $properties['available'] = array(
    'type' => 'boolean',
    'label' => t('Available'),
    'getter callback' => 'officehours_task_is_available',
    'entity views field' => TRUE,
    'description' => t('Whether a task is available'),
  );
  $properties['claimed'] = array(
    'type' => 'boolean',
    'label' => t('Claimed'),
    'getter callback' => 'officehours_task_is_claimed',
    'entity views field' => TRUE,
    'description' => t('Whether a task is claimed'),
  );
}

/**
 * Entity property getter callback: Returns a list of all participants.
 */
function officehours_task_all_participants($entity, $options, $name, $entity_type, $info) {
  $participants = array();
  // Get a wrapper for this task.
  $wrapper = entity_metadata_wrapper($entity_type, $entity);
  foreach ($wrapper->field_task_attempt as $attempt) {
    try {
      $participant = $attempt->field_task_participant->value();
      $progress = $attempt->field_progress->value();
      if (!empty($participant) && $progress == OFFICEHOURS_TASK_PROGRESS_CURRENT_WORK) {
        $participants = array_merge($participant, $participants);
      }
    }
    catch (EntityMetadataWrapperException $e) {}
  }

  $return = array();
  foreach ($participants as $participant) {
    $return[$participant->uid] = $participant;
  }

  return array_values($return);
}

/**
 * Entity property getter callback: Returns a list of all mentors.
 */
function officehours_task_all_mentors($entity, $options, $name, $entity_type, $info) {
  $mentors = array();
  // Get a wrapper for this task.
  $wrapper = entity_metadata_wrapper($entity_type, $entity);
  foreach ($wrapper->field_task_attempt as $attempt) {
    try {
      $mentor = $attempt->field_task_mentor->value();
      $progress = $attempt->field_progress->value();
      if (!empty($mentor) && $progress == OFFICEHOURS_TASK_PROGRESS_CURRENT_WORK) {
        $mentors = array_merge($mentor, $mentors);
      }
    }
    catch (EntityMetadataWrapperException $e) {}
  }

  $return = array();
  foreach ($mentors as $mentor) {
    $return[$mentor->uid] = $mentor;
  }

  return array_values($return);
}

/**
 * Entity property getter callback: Determines if a task is available.
 */
function officehours_task_is_available($entity, $options, $name, $entity_type, $info) {
  // Get a wrapper for this task.
  $wrapper = entity_metadata_wrapper($entity_type, $entity);

  // Check for a value in the followup field.
  $followup = '';
  try {
    $followup = $wrapper->field_followup->value();
  }
  catch (EntityMetadataWrapperException $e) {}

  // The task is available if no participant is currently active and the task
  // is not complete nor waiting for review.
  return (!officehours_task_is_active($wrapper) && ($followup != OFFICEHOURS_TASK_FOLLOWUP_COMPLETE) && ($followup != OFFICEHOURS_TASK_FOLLOWUP_REVIEW));
}

/**
 * Entity property getter callback: Determines if a task is claimed.
 */
function officehours_task_is_claimed($entity, array $options, $name, $entity_type, $info) {
  // Get a wrapper for this task.
  $wrapper = entity_metadata_wrapper($entity_type, $entity);

  // Find the most recent participant.
  $participant = officehours_task_get_participant($wrapper);
  if (!$participant) {
    return FALSE;
  }
  if (empty($participant->name)) {
    return FALSE;
  }
  $participant = $participant->name->value();

  // Find who the issue is assigned to.
  $assigned = $wrapper->field_task_d_o_issue->field_d_o_assigned->value();

  // @todo Perhaps we should convert to lowercase and strip special characters.
  return $participant == $assigned;
}

/**
 * Finds the most recent participant.
 *
 * @param EntityMetadataWrapper $task
 *   An entity_metadata_wrapper of the task.
 *
 * @return EntityMetadataWrapper|false
 *   Either a participant, or FALSE if there are none.
 */
function officehours_task_get_participant($task) {
  $delta = $task->field_task_attempt->count() - 1;
  $participant = $task->field_task_attempt[$delta]->field_task_participant;

  // Check for an actual participant.
  try {
    $participant->value();
  }
  catch (EntityMetadataWrapperException $e) {
    return FALSE;
  }

  return $participant;
}

/**
 * Determines if any participant is active on the task.
 *
 * @param EntityMetadataWrapper $task
 *   An entity_metadata_wrapper of the task.
 *
 * @return EntityMetadataWrapper|false
 *   Either a participant, or FALSE if there are none.
 */
function officehours_task_is_active($task) {
  // Check each attempt to determine if any are active.
  foreach ($task->field_task_attempt as $attempt) {
    try {
      $participant = $attempt->field_task_participant->value();
      $progress = $attempt->field_progress->value();
      if (!empty($participant) && $progress == OFFICEHOURS_TASK_PROGRESS_CURRENT_WORK) {
        return TRUE;
      }
    }
    catch (EntityMetadataWrapperException $e) {}
  }

  // If no attempt was active, then the task is not being worked on.
  return FALSE;
}

/**
 * Implements hook_preprocess_HOOK() for theme_field_collection_table_multiple_value_fields().
 */
function officehours_task_preprocess_field_collection_table_multiple_value_fields(&$variables) {
  // Change the text of the "add item" button.
  $variables['element']['add_more']['#value'] = t('Add new attempt');
  // Remove wrappers.
  foreach (element_children($variables['element']) as $delta) {
    // Ignore the 'add_more' child element.
    if ($delta !== 'add_more') {
      // Remove the fieldset from around date widgets.
      $langcode = $variables['element'][$delta]['field_task_date']['#language'];
      unset($variables['element'][$delta]['field_task_date'][$langcode][0]['#theme_wrappers']);
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for theme_field_collection_table_multiple_value_field().
 */
function officehours_task_preprocess_field_collection_table_multiple_value_field(&$variables) {
  // Remove the fieldset from around date widgets.
  $langcode = $variables['element']['field_task_date']['#language'];
  unset($variables['element']['field_task_date'][$langcode][0]['#theme_wrappers']);
}

/**
 * Implements hook_form_FORM_ID alter for field_collection_item_form().
 *
 * Tweaks the title for the field collection item add/edit form.
 *
 * @see http://drupal.org/node/1339294
 * @see http://drupal.org/node/1335204
 */
function officehours_task_form_field_collection_item_form_alter(&$form, &$form_state, $form_id) {
  $item = $form_state['field_collection_item'];
  if (!isset($item->is_new)) {
    drupal_set_title(
      t(
        'Edit @label for @bundle %title',
        array(
          '@label' => $item->label(),
          '@bundle' => $item->hostEntityBundle(),
          '%title' => $item->hostEntity()->title,
        )
      ),
      PASS_THROUGH
    );
  }
  else {
    drupal_set_title(
      t(
        'Add new @fc_type to @bundle %title',
        array(
          '@fc_type' => $item->translatedInstanceLabel(),
          '@bundle' => $item->hostEntityBundle(),
          '%title' => $item->hostEntity()->title,
        )
      ),
      PASS_THROUGH
    );
  }
}

/**
 * Implements hook_field_attach_form().
 */
function officehours_task_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
  // Add a title above the task attempt field collection.
  if ($entity_type == 'node' && $entity->type == 'task') {
    $form['field_task_attempt']['#prefix'] = '<label>' . check_plain($form['field_task_attempt'][$langcode]['#title']) . '</label>';
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter() for entityreference_autocomplete.
 *
 * Adds custom validation to interpret Drupal.org node IDs and automatically
 * create d_o_issue nodes when appropriate.
 */
function officehours_task_field_widget_entityreference_autocomplete_form_alter(&$element, &$form_state, $context) {
  // Only act on issue reference fields.
  if ($context['field']['field_name'] != 'field_task_d_o_issue') {
    return;
  }
  // Attach the Drupal.org issue URL converter to the node ID field.
  $element['target_id']['#attributes']['class'][] = 'change_nid';
  $element['target_id']['#attached']['js'][] =
    drupal_get_path('module', 'd_o_issue_importer') . '/d_o_issue_importer.js';

  if (!empty($element['target_id']['#element_validate'])) {
    array_unshift($element['target_id']['#element_validate'], 'officehours_task_import_d_o_issue');
  }
  else {
    $element['target_id']['#element_validate'] = array('officehours_task_import_d_o_issue');
  }
}

/**
 * Form element validation handler for task issue reference fields.
 *
 * Converts Drupal.org node IDs into the correct entityreference values and
 * automatically imports data for a given Drupal.org issue when its node ID is
 * entered in the issue reference field.
 *
 * @todo Language handling.
 */
function officehours_task_import_d_o_issue(&$element, &$form_state) {
  // Retrieve the user-entered node ID.
  // field_language() and field_get_items() fail within entity forms, and
  // I really don't feel like wasting time on this, so LANGUAGE_NONE for now.
  $langcode = LANGUAGE_NONE;
  $d_o_nid = $form_state['values']['field_task_d_o_issue'][$langcode][0]['target_id'];
  // Only act on integer values for the field.
  if (!is_numeric($d_o_nid)) {
    return;
  }

  // If the node has not yet been imported, create it.
  $imported_nid = d_o_issue_importer_get_imported_nid($d_o_nid);
  if (empty($imported_nid)) {
    global $user;
    $node = new stdClass();
    $node->type = 'd_o_issue';
    $node->uid = $user->uid;
    $node->title = 'Import d.o issue';
    $node->language = $langcode;
    $node->field_d_o_nid[$langcode][0]['value'] = $d_o_nid;
    node_save($node);
    feeds_source('d_o_json', $node->nid)->startImport();
    $imported_nid = $node->nid;
  }
  else {
    $node = node_load($imported_nid);
  }
  $autocomplete = $node->title . " ($imported_nid)";
  form_set_value($element, $autocomplete, $form_state);
  $element['#value'] = $autocomplete;
}

/**
 * Implements hook_views_data_alter().
 */
function officehours_task_views_data_alter(&$data) {
  // Switch the availble entity property to use a custom Views handler.
  $data['views_entity_node']['available']['field']['handler'] = 'officehours_task_handler_field_available';
}
