<?php
/**
 * @file
 * coh_tasks.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function coh_tasks_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'mentoring_tasks';
  $view->description = 'Provides information about office hours task types.';
  $view->tag = 'office hours';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Task information';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Task information';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Field: Taxonomy term: Level */
  $handler->display->display_options['fields']['field_level']['id'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['table'] = 'field_data_field_level';
  $handler->display->display_options['fields']['field_level']['field'] = 'field_level';
  /* Field: Taxonomy term: Queue */
  $handler->display->display_options['fields']['field_queue']['id'] = 'field_queue';
  $handler->display->display_options['fields']['field_queue']['table'] = 'field_data_field_queue';
  $handler->display->display_options['fields']['field_queue']['field'] = 'field_queue';
  $handler->display->display_options['fields']['field_queue']['click_sort_column'] = 'url';
  /* Field: Taxonomy term: Instructions */
  $handler->display->display_options['fields']['field_instructions']['id'] = 'field_instructions';
  $handler->display->display_options['fields']['field_instructions']['table'] = 'field_data_field_instructions';
  $handler->display->display_options['fields']['field_instructions']['field'] = 'field_instructions';
  $handler->display->display_options['fields']['field_instructions']['click_sort_column'] = 'url';
  /* Sort criterion: Taxonomy term: Level (field_level) */
  $handler->display->display_options['sorts']['field_level_value']['id'] = 'field_level_value';
  $handler->display->display_options['sorts']['field_level_value']['table'] = 'field_data_field_level';
  $handler->display->display_options['sorts']['field_level_value']['field'] = 'field_level_value';
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'core_mentoring_task' => 'core_mentoring_task',
  );

  /* Display: Task type chart */
  $handler = $view->new_display('page', 'Task type chart', 'page');
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_level',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'field_level' => 'field_level',
    'field_queue' => 'field_queue',
    'field_instructions' => 'field_instructions',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_level' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_queue' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_instructions' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Taxonomy term: Content using Task type */
  $handler->display->display_options['relationships']['reverse_field_task_type_node']['id'] = 'reverse_field_task_type_node';
  $handler->display->display_options['relationships']['reverse_field_task_type_node']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['reverse_field_task_type_node']['field'] = 'reverse_field_task_type_node';
  $handler->display->display_options['relationships']['reverse_field_task_type_node']['label'] = 'tasks_of_type';
  /* Relationship: Content: Sprint (field_sprint) */
  $handler->display->display_options['relationships']['field_sprint_tid']['id'] = 'field_sprint_tid';
  $handler->display->display_options['relationships']['field_sprint_tid']['table'] = 'field_data_field_sprint';
  $handler->display->display_options['relationships']['field_sprint_tid']['field'] = 'field_sprint_tid';
  $handler->display->display_options['relationships']['field_sprint_tid']['relationship'] = 'reverse_field_task_type_node';
  $handler->display->display_options['relationships']['field_sprint_tid']['label'] = 'sprint';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Field: Taxonomy term: Level */
  $handler->display->display_options['fields']['field_level']['id'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['table'] = 'field_data_field_level';
  $handler->display->display_options['fields']['field_level']['field'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['label'] = '';
  $handler->display->display_options['fields']['field_level']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_level']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Queue */
  $handler->display->display_options['fields']['field_queue']['id'] = 'field_queue';
  $handler->display->display_options['fields']['field_queue']['table'] = 'field_data_field_queue';
  $handler->display->display_options['fields']['field_queue']['field'] = 'field_queue';
  $handler->display->display_options['fields']['field_queue']['label'] = 'Where to find issues';
  $handler->display->display_options['fields']['field_queue']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_queue']['group_column'] = 'entity_id';
  /* Field: Taxonomy term: Instructions */
  $handler->display->display_options['fields']['field_instructions']['id'] = 'field_instructions';
  $handler->display->display_options['fields']['field_instructions']['table'] = 'field_data_field_instructions';
  $handler->display->display_options['fields']['field_instructions']['field'] = 'field_instructions';
  $handler->display->display_options['fields']['field_instructions']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_instructions']['group_column'] = 'entity_id';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'reverse_field_task_type_node';
  $handler->display->display_options['fields']['nid']['group_type'] = 'count';
  $handler->display->display_options['fields']['nid']['label'] = 'Available';
  $handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Taxonomy term: Term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['relationship'] = 'field_sprint_tid';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['tid']['validate_options']['vocabularies'] = array(
    'sprint' => 'sprint',
  );
  $handler->display->display_options['arguments']['tid']['validate_options']['type'] = 'convert';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'core_mentoring_task' => 'core_mentoring_task',
  );
  /* Filter criterion: Content: Task attempt (field_task_attempt) */
  $handler->display->display_options['filters']['field_task_attempt_value']['id'] = 'field_task_attempt_value';
  $handler->display->display_options['filters']['field_task_attempt_value']['table'] = 'field_data_field_task_attempt';
  $handler->display->display_options['filters']['field_task_attempt_value']['field'] = 'field_task_attempt_value';
  $handler->display->display_options['filters']['field_task_attempt_value']['relationship'] = 'reverse_field_task_type_node';
  $handler->display->display_options['filters']['field_task_attempt_value']['operator'] = 'empty';
  $handler->display->display_options['path'] = 'task-info';

  /* Display: Task info sidebar */
  $handler = $view->new_display('block', 'Task info sidebar', 'task_info');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['label'] = '';
  $handler->display->display_options['fields']['name_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name_1']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Instructions */
  $handler->display->display_options['fields']['field_instructions']['id'] = 'field_instructions';
  $handler->display->display_options['fields']['field_instructions']['table'] = 'field_data_field_instructions';
  $handler->display->display_options['fields']['field_instructions']['field'] = 'field_instructions';
  $handler->display->display_options['fields']['field_instructions']['label'] = '';
  $handler->display->display_options['fields']['field_instructions']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_instructions']['alter']['text'] = '[name_1]: Instructions';
  $handler->display->display_options['fields']['field_instructions']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_instructions']['alter']['path'] = '[field_instructions-url]';
  $handler->display->display_options['fields']['field_instructions']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_instructions']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_instructions']['type'] = 'link_plain';
  /* Field: Taxonomy term: Level */
  $handler->display->display_options['fields']['field_level']['id'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['table'] = 'field_data_field_level';
  $handler->display->display_options['fields']['field_level']['field'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['label'] = '';
  $handler->display->display_options['fields']['field_level']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['name']['alter']['text'] = 'Similar tasks';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Taxonomy term: Term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['tid']['default_argument_options']['term_page'] = FALSE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['node'] = TRUE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['limit'] = TRUE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['vocabularies'] = array(
    'core_mentoring_task' => 'core_mentoring_task',
  );
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['tid']['validate_options']['vocabularies'] = array(
    'core_mentoring_task' => 'core_mentoring_task',
  );

  /* Display: Reviewer notes */
  $handler = $view->new_display('block', 'Reviewer notes', 'reviewer_notes');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Notes for reviewers';
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    5 => '5',
    4 => '4',
    6 => '6',
  );
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Notes for reviewers */
  $handler->display->display_options['fields']['field_notes_for_reviewers']['id'] = 'field_notes_for_reviewers';
  $handler->display->display_options['fields']['field_notes_for_reviewers']['table'] = 'field_data_field_notes_for_reviewers';
  $handler->display->display_options['fields']['field_notes_for_reviewers']['field'] = 'field_notes_for_reviewers';
  $handler->display->display_options['fields']['field_notes_for_reviewers']['label'] = '';
  $handler->display->display_options['fields']['field_notes_for_reviewers']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Taxonomy term: Term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['tid']['default_argument_options']['term_page'] = FALSE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['node'] = TRUE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['limit'] = TRUE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['vocabularies'] = array(
    'core_mentoring_task' => 'core_mentoring_task',
  );
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';

  /* Display: Reviewer notes page */
  $handler = $view->new_display('page', 'Reviewer notes page', 'reviewer_notes_page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Notes for reviewers';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_level',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'field_level' => 'field_level',
    'field_queue' => 'field_queue',
    'field_instructions' => 'field_instructions',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_level' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_queue' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_instructions' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Field: Taxonomy term: Level */
  $handler->display->display_options['fields']['field_level']['id'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['table'] = 'field_data_field_level';
  $handler->display->display_options['fields']['field_level']['field'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['label'] = '';
  $handler->display->display_options['fields']['field_level']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_level']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Notes for reviewers */
  $handler->display->display_options['fields']['field_notes_for_reviewers']['id'] = 'field_notes_for_reviewers';
  $handler->display->display_options['fields']['field_notes_for_reviewers']['table'] = 'field_data_field_notes_for_reviewers';
  $handler->display->display_options['fields']['field_notes_for_reviewers']['field'] = 'field_notes_for_reviewers';
  $handler->display->display_options['path'] = 'reviewer-notes';
  $export['mentoring_tasks'] = $view;

  return $export;
}
