<?php

/**
 * Implements theme_field__field_type().
 *
 * Override Bartik's stupid taxonomy term styling.
 */
function bucket_field__taxonomy_term_reference($variables) {
  return theme_field($variables);
}
